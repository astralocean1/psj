import time
import concurrent.futures

T1 = time.perf_counter()
def do_somthing(sec):
    print(f"Diam sejenak . . . {sec} detik")
    time.sleep(sec)
    print("selesai berdiam . . .")

with concurrent.futures.ProcessPoolExecutor() as executor:
    secs = [5,4,3,2,1]
    results = executor.map(do_somthing,secs)
    for result in results:
        print(result)

T2 = time.perf_counter()
print(f"Selesai dalam ... {round(T2-T1,2)} detik")