import time
import multiprocessing
T1 = time.perf_counter()
def do_somthing():
    print("Diam sejenak . . . 1 detik")
    time.sleep(1)
    print("selesai berdiam . . .")

P1 = multiprocessing.Process(target=do_somthing)
P2 = multiprocessing.Process(target=do_somthing)
P3 = multiprocessing.Process(target=do_somthing)

P1.start()
P2.start()
P3.start()

P1.join()
P2.join()
P3.join()

T2 = time.perf_counter()
print(f"Selesai dalam ... {round(T2-T1,2)} detik")