import time
import multiprocessing
T1 = time.perf_counter()
def do_somthing():
    print("Diam sejenak . . . 1 detik")
    time.sleep(1)
    print("selesai berdiam . . .")

Process = []
for x in range(10):
    P = multiprocessing.Process(target=do_somthing)
    P.start()
    Process.append(P)

for process in Process:
    process.join()

T2 = time.perf_counter()
print(f"Selesai dalam ... {round(T2-T1,2)} detik")